def Discret(subject):
    line  = np.loadtxt('data/' + subject + '.txt')
    n = len(line)
    output = []
    i = 0
    
    
    while(i< n-1):
        value = line[i+1] - line[i]
        output.append(value)
        i+=1
    
    n = len(output)
    i = 0
    psum = 0
    nsum = 0
    j = 0
    k = 0
    
    while(i<n):
        if(line[i] > 0 ):
            psum+=line[i]
            j += 1
        else:
            nsum+=line[i]
            k += 1
        i+=1
    paverage = psum/j
    naverage = nsum/k
    
    discret = []
    i = 0
    while(i<n):
        if(line[i] > paverage):
            discret.append('A')
        elif(0 < line[i] < paverage):
            discret.append('T')
        elif(naverage <  line[i] < 0):
            discret.append('C')
        elif(line[i] < naverage):
            discret.append('G')
        else:
            print line[i]
        i+=1
    print len(discret)
    f = open('output/' + str(subject) + '_ATCG.txt','w+')
    h = 0
    while(h<len(discret)):
        f.write(discret[h])
        h+=1
    f.close()