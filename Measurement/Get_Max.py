def GetMax(scale,subject,blocklength):


	inputfolder = subject +' move'


	f = open(inputfolder +  '/r=' + str(scale) + '.txt', "r")
	line = f.readline()

	a = spliter(line,blocklength)

	length = len(a)



	x = [ i for i in xrange(blocklength+1) ]
	Pnum = [ 0 for i in xrange(blocklength+1) ]
	Qnum = [ 0 for i in xrange(blocklength+1) ]
	PQnum = [[0 for i in range(blocklength/2 +1)] for i in range(blocklength +1)]

	P = [0 for i in xrange(length+1)]
	Q = [0 for i in xrange(length+1)]


	i = 0



	while(i < length):

		quantity1 = len(re.findall("1",a[i]))
		quantity2 = len(re.findall("01",a[i]))
		if(a[i][0] =='1' and a[i][-1] =='0'): #see each block as a loop structure
			quantity2 += 1
		P[i] = quantity1
		Q[i] = quantity2
		Pnum[quantity1] += 1
		Qnum[quantity2] += 1
		PQnum[quantity1][quantity2] += 1
		i+=1


	j = 0
	k = 1
	while(j < length):
		Qnext[j] = Q[k]
		Pnext[j] = P[k]
		j += 1
		k += 1


	i = 0
	while(i < (len(Pnum)-1)):
		maxP = float(max(Pnum)) / length
		maxQ = float(max(Qnum)) / length
		i+=1

	maxPQ = 0
	j = 0
	while(j < len(PQmax)):
		if(maxPQ < max(PQmax[j])):
			maxPQ = float(max(PQnum[j]))
		j += 1
	maxPQ = maxPQ / len(a)
	

	print ("r = " + str(scale) + ' '  +  str(maxP) + '  '  + str(maxQ) +  '  ' + str(maxPQ))

	f.close()


	return maxP,maxQ,maxPQ
